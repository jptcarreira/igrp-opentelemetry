# Install IGRP with opentelemetry enabled
This is a basic IGRP project to test the opentelemetry agent. The graphical user interface used is the zipkin.

#### Index  
---
[Prepare the IGRP version](#prepare-the-igrp-version)  
[Install and start zipkin docker image](#install-and-start-zipkin-docker-image)  
[Checkout this sample application with opentelemetry enabled](#checkout-this-sample-application-with-opentelemetry-enabled)  
[Start this demo application](#start-this-demo-application)  
[Using other agents](#using-other-agents)  
[Using elastic search as log appender](#using-elastic-search-as-log-appender)  

---

## Prepare the IGRP version
First we need to checkout a specific branch of IGRP prepared to work with mvn archetype. This is a specific branch of the project `igrpweb`

```bash
# Clone out the branch of IGRP
git clone --branch igrp-horizon-dev-modules http://git.nosi.cv/igrp-web/igrp-web-framework.git

# Install IGRP on maven (.m2) local repository
cd igrp-web-framework

# ( Linux / macos )
./mvnw install

# ( Windows )
mvnw.cmd install

```

This commands will install two artifacts in the repository. The IGRP jar with all the java dependencies, and the WAR file with all web resoruces needed for a IGRP application

>After this step we can use the maven dependencies in IGRP new projects. In this example, we will not go into that details.


## Install and start zipkin docker image

```bash
# Install and start zipkin docker image in daemon mode.
docker run -d -p 9411:9411 openzipkin/zipkin
```

## Checkout this sample application with opentelemetry enabled
```bash

# Use a diferent folder from igrp-framework
cd ..

# Clone the sample application
git clone https://gitlab.com/jptcarreira/igrp-opentelemetry.git

```

This application as the configuration of opentelemetry in the pom.xml
They are defaulted to zipkin and the export url is http://localhost:9411/api/v2/spans

This values can be ajusted as needed.

This is the pom.xml configured with the defaults

```xml
				<plugin>
					<groupId>org.apache.tomee.maven</groupId>
					<artifactId>tomee-maven-plugin</artifactId>
					<version>${tomee.maven.plugin.version}</version>
					<configuration>
						<javaagents>
							<javaagent>${project.basedir}/opentelemetry-agent/opentelemetry-javaagent.jar</javaagent>
						</javaagents>
						<encoding>UTF-8</encoding>
						<tomeeClassifier>plus</tomeeClassifier>
						<reloadOnUpdate>true</reloadOnUpdate>
						<args>-Dfile.encoding=UTF-8 -Dotel.resource.attributes=service.name=irgp-opentelemetry -Dotel.metrics.exporter=none -Dotel.exporter.zipkin.endpoint=http://localhost:9411/api/v2/spans -Dotel.traces.exporter=zipkin</args>
						<synchronization>
					      <extensions>
					        <extension>.class</extension> <!-- if you want to update each time you build with mvn compile -->
					        <extension>.properties</extension>
					        <extension>.xml</extension>
					      </extensions>
					    </synchronization>
					    <systemVariables>
							<tomee.serialization.class.whitelist />
							<tomee.serialization.class.blacklist>-</tomee.serialization.class.blacklist>
							<openejb.system.apps>true</openejb.system.apps>
							<tomee.remote.support>true</tomee.remote.support>
						</systemVariables>
					</configuration>
				</plugin>
```

- Tag `javaagent` constain the location of opentelemetry agent file. 
- Tag `args` application specific configuration. This is where we can and the custom parameters with diferent options for opentelemetry

> This options can be configured in environment variables, as documented oficial opentelemetry java agent (Agent Configuration)[https://opentelemetry.io/docs/instrumentation/java/automatic/agent-config/]


## Start this demo application

This demo application, uses the embebed tomee plug in to run. If this is to be installed in a server, a diferent aproach shoud be considered. For demonstration proposes, is simple and only depends on maven

```bash
# Run the maven application

# (Linux / macos)
./mvnw package tomee:run

# ( Windows )
mvnw.cmd package tomee:run

```

## Check the traces in opentelemetry

After the previous step, the application starts to publish the traces to zipkin. They can be acessed with the following url:
http://127.0.0.1:9411/zipkin

Screen shot of a request in zipkin
![Screen Shot](images/zipkin-ss.png)


## Using other agents

For this step, we sugest to use a `.properties` file.

Editing the pom.xml to use the properties file:
- Change the `args` to `-Dfile.encoding=UTF-8 -Dotel.javaagent.configuration-file=${project.basedir}/uptrace.properties`
- Ajust the file `uptrace.properties` with your specific configurations

Example of the `uptrace.properties`
```properties
otel.traces.exporter=otlp
otel.metrics.exporter=otlp
otel.logs.exporter=otlp
otel.exporter.otlp.endpoint=https://otlp.uptrace.dev:4317
otel.exporter.otlp.headers=uptrace-dsn={{ dsn }}
otel.resource.attributes=service.name=irgp-opentelemetry,service.version=1.0.0
```

>Check the documentation at https://github.com/open-telemetry/opentelemetry-java
Example of the `pom.xml` configured with properties file.

```xml
                <!-- Embedded TomEE plugin -->
				<plugin>
					<groupId>org.apache.tomee.maven</groupId>
					<artifactId>tomee-maven-plugin</artifactId>
					<version>${tomee.maven.plugin.version}</version>
					<configuration>
						<javaagents>
							<javaagent>${project.basedir}/opentelemetry-agent/opentelemetry-javaagent.jar</javaagent>
						</javaagents>
						<encoding>UTF-8</encoding>
						<tomeeClassifier>plus</tomeeClassifier>
						<reloadOnUpdate>true</reloadOnUpdate>
						<args>-Dfile.encoding=UTF-8 -Dotel.javaagent.configuration-file=${project.basedir}/uptrace.properties</args>
						<synchronization>
					      <extensions>
					        <extension>.class</extension> <!-- if you want to update each time you build with mvn compile -->
					        <extension>.properties</extension>
					        <extension>.xml</extension>
					      </extensions>
					    </synchronization>
					    <systemVariables>
							<tomee.serialization.class.whitelist />
							<tomee.serialization.class.blacklist>-</tomee.serialization.class.blacklist>
							<openejb.system.apps>true</openejb.system.apps>
							<tomee.remote.support>true</tomee.remote.support>
						</systemVariables>
					</configuration>
				</plugin>
```

## Using elastic search as log appender

This configuration, enable the elastic search appender.
To configure the appender locate the file `src/main/resources/log4j2.xml` and uncoment the lines.

Example of ELS `log4j2.xml` configuration:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="TRACE">
  <Appenders>
    <Console name="Console" target="SYSTEM_OUT">
      <PatternLayout pattern="%d{yyyy-MM-dd HH:mm:ss.SSS} [%t] %x %X %-5level %logger{36}:%L - %msg%n"/>
    </Console>
    <Elasticsearch name="elasticsearchAsyncBatch">
        <IndexName indexName="logs-irgp-opentelemetry" />
        <JacksonJsonLayout />
        <AsyncBatchDelivery batchSize="1000" deliveryInterval="5000" >
            <!-- IndexTemplate name="log4j2-irgp-teste" path="classpath:indexTemplate.json" /--> <!-- Template for index generation (Not working) -->
            <JestHttp serverUris="http://localhost:9200" />
        </AsyncBatchDelivery>
    </Elasticsearch>
  </Appenders>
  <Loggers>
  	<Logger name="nosi" level="debug" additivity="false">
      <AppenderRef ref="Console"/>
      <AppenderRef ref="elasticsearchAsyncBatch"/>
    </Logger>
    <Root level="info">
      <AppenderRef ref="Console"/>
	  <AppenderRef ref="opentelemetry"/>      
    </Root>
  </Loggers>
</Configuration>

```
